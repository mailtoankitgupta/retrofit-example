package com.example.ankit.retrofittest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface FotoniciaService {
    @GET("V1/categories/{categoryId}/products")
    Call<List<CategoryProduct>> getProductsByCategory(@Path("categoryId") String categoryId);
}
