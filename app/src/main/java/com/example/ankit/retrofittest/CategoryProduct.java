package com.example.ankit.retrofittest;

import com.google.gson.annotations.SerializedName;

class CategoryProduct {
    String sku;
    int position;
    @SerializedName("category_id")
    String categoryId;
    @SerializedName("extension_attributes")
    CategoryProductExtension extensions;

    static class CategoryProductExtension {
        String name;
        @SerializedName("minimal_price")
        float minimalPrice;
        String image;
        String description;
    }
}
