package com.example.ankit.retrofittest;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dev.fotonicia.in/rest/default/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                FotoniciaService service = retrofit.create(FotoniciaService.class);

                Call<List<CategoryProduct>> productCall = service.getProductsByCategory("2");

                Response<List<CategoryProduct>> response = null;
                try {
                    response = productCall.execute();
                    if (response.isSuccessful()) {
                        List<CategoryProduct> products = response.body();
                        if (products != null) {
                            Log.v("MainActivity", "Products received: " + products.size());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }
}
